# Seafile forms (SeafForm)

This is a tool to produce forms from an ODS Spreasheet (LibreOffice) put in a 
Seafile library.
Results will be saved directly into the spreadsheet.

## Examples

You can see SeafForm in action with some demonstration forms:
  
  - Training course satisfaction
    - The form: https://forms.tila.im/form/training-course-satisfaction-1/ 
    - The spreadsheet: https://docs.tila.im/f/32d0c74abab541349f74/?dl=1 
  
  - Our next meeting
    - The form: https://forms.tila.im/form/our-next-meeting-1/
    - The spreadsheet: https://docs.tila.im/f/0274304a3b8f4619a471/?dl=1
  
  - Camping hollidays
    - The form: https://forms.tila.im/form/camping-hollidays-1/
    - The spreadsheet: https://docs.tila.im/f/52ef8ad4457541688c05/?dl=1
  
  - Hiring table
    - The form: https://forms.tila.im/form/hiring-table-for-next-training-courses-1/
    - The spreadsheet: https://docs.tila.im/f/3d7491733e9c4afc8727/?dl=1

  - Form with a link to come back and edit your data
    - The form: https://forms.tila.im/form/confidential-data-1/
    - The spreadsheet: https://docs.tila.im/f/ad29dc08c9f041e8ad5e/?dl=1


Screenshot of the administration: https://framagit.org/florencebiree/seafform/blob/master/doc/private-screenshot.png

## How to use it

You must have access to a Seafile instance. Then follow INSTALL.md instructions 
to install SeafForm in your server. You will need Python 3 and Django:
https://framagit.org/florencebiree/seafform/blob/master/INSTALL.md

SeafForm is released under the terms of the Affero General Public License (v3+),
so you must show a link to the source code to all your users. Read the COPIYING
file for more informations.

## How to upgrade

Read upgrading instructions here:
https://framagit.org/florencebiree/seafform/-/blob/master/UPGRADING.md

## Manage.py commands

After the Seafform installation, two special management commands are available:

To define an existing user as administrator:

    ./manage.py set_admin email_id

To migrate an user from one email_id to another (like the migrate Seafile API)

    ./manage.py migrate_user from_email to_email
    
(you muste `activate` the virtual env before using this commands)
