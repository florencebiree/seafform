��    l      |  �   �      0	  ?   1	  Y   q	  �   �	  W   �
  �   �
  |  �  v   B  W   �  P     Z   b  I   �  W     �   _  �   �  �   o  _     	   ~  (   �     �     �     �     �     �     �  $        &     =     O     a  /   h     �     �     �  	   �     �  5   �  B     J   \  F   �     �     �  
          (   &     O     U  %   \     �     �     �     �     �  $   �  N   �     ;     I     f     �  %   �     �     �  .   �       #        2  C   P  H   �  M   �     +     I     O     a     h  '   {     �     �     �     �     �     �     �     �     �       	                  !     *     3     8     ;     B     H     T     h     {     �     �     �     �     �     �     �     �     �     �  E  �     6  B   J  �   �  _     �   |  8  K  y   �  b   �  ?   a  d   �  O     ^   V  t   �  �   *   �   �   c   p!  	   �!  '   �!     "      %"     F"     M"     U"     d"  -   l"     �"     �"     �"  	   �"  >   �"     5#  '   N#     v#     �#     �#  T   �#  N   �#  ]   C$  ]   �$     �$     %     %  &   +%  F   R%     �%     �%  8   �%     �%  &   �%     &&     5&     B&  8   V&  e   �&     �&  &   	'  (   0'     Y'  (   k'     �'  .   �'  >   �'     
(  '   (  "   :(  g   ](  T   �(  Z   )  !   u)     �)     �)     �)     �)  5   �)  
   *     #*     0*     <*     W*     n*     s*     *  
   �*     �*     �*     �*     �*     �*  	   �*     �*     �*     +     
+     +     *+  '   D+     l+     �+     �+     �+     �+     �+     �+     �+     �+     �+     �+     $                       ]      P   N       
               1   .   :       O                  @              `   _      2       T   U   Y   d      V       4       >   '   b   R   S          "          7                6       F   W              X   l   H       8           G   k   A   &   #         ^          \   K   <   !   5   =           e   M          L      c   )           f   I   *   g          (   ,   J   %   Z       a          9   Q   3   +       j       E          D      B         ;   /   C          	   -   ?   [   i                      0   h        
                        %(nb)s answer
                         
                    This answer is canceled, and doesn't count anymore.
                 
                You must have a Seafile account on
                <a href="%(seaf_root)s">%(seaf_root)s</a>, and use the same
                credentials to log in.
                 
            An error append server-side. May be a form is not well-built?
             
            Easily create a form from a 
            <a href="https://www.libreoffice.org/">LibreOffice</a> spreadsheet
            and directly get results in <a href="http://www.seafile.com/"
            >Seafile</a>!
             
            It was a form build with
            <a href="https://en.wikipedia.org/wiki/Free_software">free
            softwares</a>: <a href="https://www.libreoffice.org/">LibreOffice</a>,
            <a href="http://seafile.com">Seafile</a> and 
            <a href="https://framagit.org/florencebiree/seafform">SeafForm (give
            me the source code)</a>!
             
            The form you try to open is no more reachable.
            May be its owner put it off-line?
             
            You can contact the form owner to tell her you had a problem.
             
        <span class="required"></span> : <em>champs obligatoires.</em>
         
        Once uploaded in Seafile, use this button to choose your form .ods file.
         
        Send the form's public address to all affected persons!
         
        The form <q>%(deleted)s</q> was disabled. Its public link is deleted.
         
        The form <q>%(title)s</q> was sucessfully created! Share the
        following address with interseted people:
         
        The title, the description and the public status of the form
        <q>%(updated)s</q> is now updated from the spreadsheet.
         
        You first need to download a template, save it in a directory
        synchronized in Seafile, and edit it to define form parameters and 
        questions.
         
    Choose in your Seafile the LibreOffice spreadsheet (.ods) which will be
    the form:
     (maximum) 1. create the spreadsheet with questions 2. select the form 3. ask people to answer Action Address Administration Cancel Check this box to cancel your answer Conexion to Seafile... Copy to Clipboard Create a new form Delete Delete an answer is not authorized in this form Delete this answer Download a form template Edit the form Error is: Error: Failed to create the subdir {0} to put files for {1}. Field "{}": the parameter must contain something like 'subdir,B+C' Field "{}": you must set a colomun name in the parameter (like 'subdir,B') Field "{}": you must set a subdir in the parameter (like 'subdir,B+C') Form not found Info: Loading... Log in to create a form Log in to view forms or create a new one Login Logout Missing parameter for the list at {0} My forms No public form available Open Seafile Password Public forms Save this link to come back and edit Seafile authentication invalid. The owner of the form need to re-authenticate. Seafile forms Seafile forms administration Select the form in Seafile Server fault So long, and thanks for all the fish! Submit Survey? Poll? Inscriptions? Thank you for taking time to answer this form! Thanks! The description cell is empty ({0}) The field in {0} need a name. The form file miss the 'Data' tab. Is it really built from a model? The maximum number of answer ({0}) is reached, you cannot add a new one. The maximum number of answer ({0}) is reached, you cannot un-cancel this one. The title cell is empty ({0}) Title Toggle navigation Update Username or e-mail You must follow this steps, one by one: cancel check checked creation date and time credentials not valid date description editlink form form id form path forms list login id longtext name no number owner public form seafile edition url seafile library id seafile library name seafile server seafile token static table text title upload user contact email yes yes + delete Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: Florence Birée <florence@biree.name>
Language-Team: French
Language: French
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 
%(nb)s réponses
  
Cette réponse a été annulée, et n'est plus prise en compte.
  
Vous devez avoir un compte sur le Seafile <a href="%(seaf_root)s">%(seaf_root)s</a>, et utiliser les mêmes identifiants pour vous connecter. 
Le serveur a rencontré une erreur. Peut-être que le formulaire n'était pas bien construit ? 
Créez simplement un formulaire à partir d'un tableur <a href="https://fr.libreoffice.org/">LibreOffice</a>, et récupérez directement les résultats dans <a href="http://www.seafile.com/" >Seafile</a> ! 
C'était un formulaire produit avec les <a href="https://fr.wikipedia.org/wiki/Logiciel_libre">logiciels libres</a> <a href="https://fr.libreoffice.org/">LibreOffice</a>, <a href="http://seafile.com">Seafile</a> et <a href="https://framagit.org/florencebiree/seafform">SeafForm (donnez-moi le code source)</a> ! 
Le formulaire que vous essayez d'ouvrir n'est plus accessible.
Peut-être que son·sa propriétaire l'a mis hors-ligne ? 
Vous pouvez contacter le propriétaire du formulaire pour lui dire que vous avez eu un problème. 
<span class="required"></span> : <em>champs obligatoires.</em> 
Une fois chargé dans Seafile, utilisez ce bouton pour choisir le fichier .ods de votre formulaire. 
Envoyez l'adresse publique du formulaire à toutes les personnes concernées ! 
Le formulaire <q>%(deleted)s</q> a bien été désactivé. Son lien public a été supprimé. 
Le formulaire <q>%(title)s</q> a été créé avec succès ! Communiquez l'adresse suivante à vos correspondants : 
       Le titre, la description et le statut public du formulaire
       <q>%(updated)s</q> sont maintenant mis à jour depuis le tableur.
        
Téléchargez tout d'abord un modèle, enregistrez-le dans un répertoire synchronisé avec Seafile, et éditez-le pour définir les paramètres et les questions du formulaire. 
Sélectionnez dans votre Seafile le tableur LibreOffice (.ods) qui sera à la base du formulaire : (maximum) 1. créer le tableur avec les questions 2. sélectionnez le formulaire 3. invitez vos amis à répondre Action Adresse Administration Annuler Cochez cette case pour annuler votre réponse Connexion à Seafile... Copier dans le presse-papier Créer un nouveau formulaire Supprimer Supprimer une réponse n'est pas authorisé dans ce formulaire Supprimer cette réponse Télécharger un modèle de formulaires Modifier le formulaire Erreur : Erreur : Erreur lors de la création du sous-dossier {0} pour y mettre les fichiers pour {1}. Champ "{}" : le paramètre doit contenir quelque chose comme 'sousdossier,B+C' Champ "{}" : vous devez définir un nom de colonne dans le paramètre (comme 'sousdossier,B') Champ "{}" : vous devez définir un sous dossier dans le paramètre (comme 'sousdossier,B+C') Formulaire introuvable Info : Chargement... Se connecter pour créer un formulaire Se connecter pour voir les formulaires publics ou en créer un nouveau Se connecter Se déconnecter Les paramètres de la liste manquent dans la cellule {0} Mes formulaires Pas de formulaires publics disponibles Ouvrir Seafile Mot de passe Formulaires publics Enregistrez ce lien pour revenir faire des modifications L'authentification sur Seafile est invalide. Le propriétaire du formulaire doit se ré-authentifier. Formulaires Seafile Administration des formulaires Seafile Sélectionnez le formulaire dans Seafile Erreur du serveur Salut, et encore merci pour le poisson ! Valider Un sondage ? Une enquête ? Des inscriptions ? Merci d'avoir pris le temps de répondre à ce questionnaire ! Merci ! La cellule 'description' est vide ({0}) Le champ en {0} a besoin d'un nom. L'onglet 'Data' manque dans le fichier du formulaire. A-t-il bien été créé à partir d'un modèle ? Le nombre maximum de réponses ({0}) est atteint, vous ne pouvez pas en ajouter une. Le nombre maximum de réponses ({0}) est atteint, vous ne pouvez pas désannuler celle-ci. La cellule 'titre' est vide ({0}) Titre Basculer la navigation Mettre à jour Nom d'utilisateurice ou e-mail Vous devez suivre ces étapes, l'une après l'autre : annulation caseàcocher casecochée date et heure de création identifiants invalides date description lienédition formulaire identifiant du formulaire chemin du formulaire formulaires liste identifiant de connexion textelong nom non nombre propriétaire formulaire public URL d'édition de seafile identifiant de la bibliothèque seafile nom de la bibliothèque seafile serveur seafile token de seafile statique tableau texte titre envoifichier e-mail de contact oui oui + suppression 