# -*- coding: utf-8 -*-
###############################################################################
#       seafform/views.py
#       
#       Copyright © 2020-2023, Florence Birée <florence@biree.name>
#       
#       This file is a part of seafform.
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as 
#       published by the Free Software Foundation, either version 3 of the 
#       License, or (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU Affero General Public License for more details.
#       
#       You should have received a copy of the GNU Affero General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Seafform views"""

__author__ = "Florence Birée"
__version__ = "0.7.1"
__license__ = "AGPLv3"
__copyright__ = "Copyright © 2023, Florence Birée <florence@biree.name>"

import os
from urllib.parse import quote, unquote, urljoin
import itertools
from django.db import IntegrityError
from django.utils.text import slugify
from django.urls import reverse
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from django.template import RequestContext
from django.utils.datastructures import MultiValueDictKeyError
from seafform.models import SeafileUser, Form
from seafform.forms import LoginForm, DjForm
from seafform.seafile_webapi import SeafileClient, AuthError, APIError, InvalidToken, AuthError
from seafform.seafform import SeafForm, HEADERS_ROW, InvalidODS
from django.utils.translation import gettext as _
from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile

# load settings or defaults
try:
    LOGO32 = settings.LOGO32
except AttributeError:
    LOGO32 = settings.STATIC_URL + '/seafform/images/seafform-owl-32px.png'
try:
    LOGO150 = settings.LOGO150
except AttributeError:
    LOGO150 = settings.STATIC_URL + '/seafform/images/seafform-owl-150px.png'
try:
    LOGO300 = settings.LOGO300
except AttributeError:
    LOGO300 = settings.STATIC_URL + '/seafform/images/seafform-owl-300px.png'
LOGOS = {32: LOGO32, 150: LOGO150, 300: LOGO300}
try:
    ROOT_DIR = '/' + settings.ROOT_DIR
except:
    ROOT_DIR = '/'
try:
    SEAFILE_SESSION_LOGIN = settings.SEAFILE_SESSION_LOGIN
except AttributeError:
    SEAFILE_SESSION_LOGIN = False
try:
    V12AUTH = settings.V12AUTH
except AttributeError:
    V12AUTH = True

def _log(request, email, password, nextview):
    """ Authenticate or create a new account """
    seaf_root = settings.SEAFILE_ROOT
        
    user = authenticate(username=email, password=password)
    # if known user:
    if user is not None and user.is_active:
        login(request, user)
        # check the token validity
        seafu = user.seafileuser                               
        seaf = SeafileClient(seafu.seafroot, verifycerts=settings.VERIFYCERTS, v12auth=V12AUTH)
        # if wrong token, will raise an AuthError, we want the user to re-log 
        # with it's new password
        try:
            seaf.authenticate(user.email, token=seafu.seaftoken, validate=True)
        except AuthError:
            logout(request)
            raise AuthError
        # get user profile to update seafileuser
        seafu.update(seaf.user_my_profile())
        
        return HttpResponseRedirect(reverse(nextview))
    
    if user is not None: # not active
        raise AuthError
    else:
        # try to connect to seafile using credentials
        seaf = SeafileClient(seaf_root, verifycerts=settings.VERIFYCERTS, v12auth=V12AUTH)
        seaf.authenticate(email, password) # may raise AuthError
        token = seaf.token
        # get user profile
        seafile_profile = seaf.user_my_profile()
        # we replace email by the one given by seafile
        email = seafile_profile['email']
        
        # create new user, save the token / or update existing user
        try:
            user = User.objects.create_user(email, email, password)
        except IntegrityError:
            # the user already exist, we want to update it
            user = User.objects.get(username = email)
            user.set_password(password)
            user.save()
            seafu = user.seafileuser
            seafu.seaftoken = token
            seafu.update(seafile_profile, save=False)
            seafu.save()
        else:
            # new user
            user.save()
            seafuser = SeafileUser(user=user, seafroot=seaf_root,
                                seaftoken=token)
            seafuser.update(seafile_profile, save=False)
            seafuser.save()
        # login
        user2 = authenticate(username=email, password=password)
        login(request, user2)
        # -> nextview
        return HttpResponseRedirect(reverse(nextview))

def _log_by_seafile_session(request):
    """ Authenticate or create a new account, by using Seafile session """
    seaf_root = settings.SEAFILE_ROOT
    
    # only try if a 'sessionid' cookie is set
    if not 'sessionid' in request.COOKIES:
        return
    
    # try to connect to seafile using cookies
    seaf = SeafileClient(seaf_root, verifycerts=settings.VERIFYCERTS, v12auth=V12AUTH)
    try:
        seaf.authenticate_cookies(request.COOKIES) # may raise AuthError
    except AuthError:
        return
    token = seaf.token
    if not token:
        return
    # get user profile
    seafile_profile = seaf.user_my_profile()
    email = seafile_profile['email']
        
    # create new user, save the token / or update existing user
    try:
        user = User.objects.create_user(email, email, token)
    except IntegrityError:
        # the user already exist, we want to update it
        user = User.objects.get(username = email)
        user.set_password(token) # we use the token as password… re-defined each time
        user.save()
        seafu = user.seafileuser
        seafu.seaftoken = token
        seafu.update(seafile_profile, save=False)
        seafu.save()
    else:
        # new user
        user.save()
        seafuser = SeafileUser(user=user, seafroot=seaf_root,
                               seaftoken=token)
        seafuser.update(seafile_profile, save=False)
        seafuser.save()
    # login
    user = authenticate(username=email, password=token)
    login(request, user)

def index(request):
    """Main login view"""
    justlogout = False
    autherror = False
    
    # if not authenticated and SEAFILE_SESSION_LOGIN, check if we can
    # authenticate with existing cookies
    if not request.user.is_authenticated and SEAFILE_SESSION_LOGIN:
        _log_by_seafile_session(request)
       
    # if authenticated, redirect to /private and no public forms
    if not settings.ALLOW_PUBLIC and request.user.is_authenticated:
        return HttpResponseRedirect(reverse('private'))
    
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = LoginForm(request.POST)
        # check whether it's valid:
        if form.is_valid():


            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            
            nextstep = (
                'index'
                if (settings.ALLOW_PUBLIC and settings.PUBLIC_NEED_AUTH) 
                else 'private'
            )
            
            try:
                return _log(request, email, password, nextstep)
            except AuthError:
                autherror = True

    # if a GET (or any other method) we'll create a blank form
    else:
        form = LoginForm()
    
    if 'action' in request.GET:
        justlogout = (request.GET['action'] == 'logout')

    return render(request, 'seafform/index.html', {
        'LOGOS': LOGOS,
        'loginform': form,
        'autherror': autherror,
        'justlogout': justlogout,
        'seaf_root': settings.SEAFILE_ROOT,
        'allow_public': settings.ALLOW_PUBLIC,
        'public_needauth': settings.PUBLIC_NEED_AUTH,
        'authenticated': request.user.is_authenticated,
        'public_forms': Form.objects.filter(public=True).\
                              order_by('-creation_datetime'),
        'show_public': (
            settings.ALLOW_PUBLIC and ( 
                request.user.is_authenticated
                or 
                not settings.PUBLIC_NEED_AUTH
            )),
        'admin_msg': settings.ADMIN_MSG,
        'seafile_session_auth': SEAFILE_SESSION_LOGIN,
        'seafile_login_url': (
            settings.SEAFILE_ROOT + 'accounts/login/?next=' + ROOT_DIR
        ),
    })

@login_required(login_url='index')
def private(request):
    """Home of private pages"""
    newform = None
    delformtitle = None
    deleted = None
    updated = None
    if request.method == 'POST': #delete or update
        if 'deleteid' in request.POST:
            deleteid = request.POST.get('deleteid', '')
            try:
                tobedeleted = Form.objects.get(formid=deleteid, owner=request.user)
            except Form.DoesNotExist:
                # do nothing
                pass
            else:
                # delete, redirect plus message
                deleted = tobedeleted.title
                tobedeleted.delete()
        elif 'updateid' in request.POST:
            updateid = request.POST.get('updateid', '')
            # get the form object
            try: 
                form = Form.objects.get(formid=updateid) 
            except Form.DoesNotExist:
                # do nothing
                pass
            else:
                # get the seafform object (Seafile connexion) 
                if settings.LOCAL: 
                    seaf = None 
                else: 
                    seafu = form.owner.seafileuser 
                    seaf = SeafileClient(seafu.seafroot, verifycerts=settings.VERIFYCERTS, v12auth=V12AUTH) 
                    seaf.authenticate(form.owner.email, token=seafu.seaftoken, validate=False) 
                seafform = SeafForm(form.filepath, seaf, form.repoid) 
                try: 
                    seafform.load() 
                except APIError: 
                    pass
                else:
                    _update_form_attr(form, seafform)
                    updated = form.title
    
    if settings.SEAFILE_OFFICE_EDIT:
        # check if all forms have edit_url
        forms = Form.objects.filter(edit_url="")
        seafu = request.user.seafileuser
        seaf = SeafileClient(seafu.seafroot, verifycerts=settings.VERIFYCERTS, v12auth=V12AUTH)
        for form in forms:
            form.edit_url = seaf.internal_file_link(form.repoid, form.filepath)
            form.save()
    
    if 'newform' in request.GET:
        try:
            newform = Form.objects.get(formid=request.GET['newform'], 
                                        owner=request.user)
        except Form.DoesNotExist:
            pass
        
    return render(request, 'seafform/private.html', {
        'LOGOS': LOGOS,
        'user': request.user,
        'forms': None,
        'tplurl': settings.TPL_URL,
        'forms': Form.objects.filter(owner=request.user).\
                              order_by('-creation_datetime'),
        'newform': newform,
        'deleted': deleted,
        'updated': updated,
        'allow_public': settings.ALLOW_PUBLIC,
        'allow_edit': settings.SEAFILE_OFFICE_EDIT,
        'admin_msg': settings.ADMIN_MSG,
    })

def logout_view(request):
    """Just… log out"""
    logout(request)
    # if we use SEAFILE_SESSION_LOGIN, also disconnect in Seafile
    if SEAFILE_SESSION_LOGIN:
        return HttpResponseRedirect(
            settings.SEAFILE_ROOT + 'accounts/logout/?next=' + ROOT_DIR 
        )
    else:
        # Redirect to a success page.
        return HttpResponseRedirect(reverse('index') + '?action=logout')

@login_required(login_url='index')
def new(request):
    """Create a new form"""
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        path = unquote(request.POST.get('path', ''))
        if path.endswith('.ods'):
            seaf = None
            if settings.LOCAL:
                filepath = os.path.join(settings.LOCAL_ROOT, path.lstrip('/'))
                seaf = None
                repoid = 'LOCAL'
                reponame = 'LOCAL'
            else:
                # Connect to Seafile
                seafu = request.user.seafileuser
                seaf = SeafileClient(seafu.seafroot, verifycerts=settings.VERIFYCERTS, v12auth=V12AUTH)
                seaf.authenticate(request.user.email, token=seafu.seaftoken, validate=False)
                # retreive path info
                parsed_path = parse(path)
                filepath = parsed_path['path']
                repoid = repo_id_from_name(seaf, parsed_path['repo_name'])
                reponame = parsed_path['repo_name']
            # load the form
            seafform = SeafForm(filepath, seaf, repoid)
            try:
                seafform.load()
            except InvalidODS as inv:
                return handler500(request, inv.msg)
            # create the slug/formid
            max_length = Form._meta.get_field('formid').max_length
            formid = orig = slugify(seafform.title)[:max_length]
            
            for x in itertools.count(1):
                if not Form.objects.filter(formid=formid).exists():
                    break
                # Truncate the original slug dynamically. Minus 1 for the hyphen.
                formid = "%s-%d" % (orig[:max_length - len(str(x)) - 1], x)
            
            # add the new form
            newform = Form(
                owner = request.user,
                filepath = filepath,
                repoid = repoid,
                reponame = reponame,
                formid = formid,
                title = seafform.title,
                creation_datetime = timezone.now(),
                description = seafform.description,
                public = seafform.public,
            )
            if settings.SEAFILE_OFFICE_EDIT and seaf:
                newform.edit_url = seaf.internal_file_link(repoid, filepath)
            newform.save()
            # Redirect + message
            return HttpResponseRedirect(reverse('private') + '?newform=' + formid)
    
    return render(request, 'seafform/new.html', {
        'user': request.user,
        'allow_public': settings.ALLOW_PUBLIC,
    })

# utility function
def parse(seafpath):
    """Return a seafile path under the form :
            {
                'repo_name':
                'path'
            }
        the root of all libraries has repo_id == None 
    """
    seafpath = seafpath.strip('/').split('/')
    if seafpath == ['']:
        # root
        return {'repo_name': None, 'repo_id': None, 'path': None}
    else:
        return {
            'repo_name': seafpath[0],
            'path': '/' + '/'.join(seafpath[1:])
        }

def repo_id_from_name(seaf, repo_name):
    """Return the repo_id from a repo_name"""
    repo_list = seaf.list_repos()
    for repo in repo_list:
        if repo['name'] == repo_name:
            return repo['id']
    return None

@csrf_exempt # the javascript lib user POST, but no data changes here
@login_required(login_url='index')
def lsdir(request):
    """Return the list of files in a Seafile directory"""
    if request.method == 'POST':
        # dirty hack
        path = unquote(unquote(request.POST['dir'], encoding='latin9'))
        if settings.LOCAL:
            abspath = settings.LOCAL_ROOT.rstrip('/') +  path
            result = [
                {
                    'name': name,
                    'type': (
                        'dir' if os.path.isdir(os.path.join(abspath, name))
                        else 'file'),
                    'path': (
                        os.path.join(abspath, name)[len(settings.LOCAL_ROOT.rstrip('/')):] +
                        ('/' if os.path.isdir(os.path.join(abspath, name)) else '')
                    )
                } for name in os.listdir(abspath)
                  if (os.path.isdir(os.path.join(abspath, name)) 
                        or name.endswith('.ods'))
            ]
        else:
            # Connect to Seafile
            seafu = request.user.seafileuser
            seaf = SeafileClient(seafu.seafroot, verifycerts=settings.VERIFYCERTS, v12auth=V12AUTH)
            seaf.authenticate(request.user.email, token=seafu.seaftoken, validate=False)
            # list the directory
            parsed_path = parse(path)
            # root of all libraries
            if parsed_path['repo_name'] is None:
                repo_list = seaf.list_repos()
                result = [
                    {
                        'name': repo['name'],
                        'type': repo['type'],
                        'path': quote('/%s/' % repo['name']),
                   } for repo in repo_list 
                ]
            else:
                repo_name, dirpath = parsed_path['repo_name'], parsed_path['path']
                repo_id = repo_id_from_name(seaf, repo_name)
                ls = seaf.list_dir(repo_id, dirpath)
                result = [
                    {
                        'name': node['name'],
                        'type': node['type'],
                        'path': quote( (
                            path.rstrip('/') + '/' + node['name'] + 
                            ('/' if node['type'] == 'dir' else '') )
                        )
                    } for node in ls
                    if (node['type'] == 'dir' or node['name'].endswith('.ods'))
                ]
        return render(request, 'seafform/lsdir.html', {'result': result})
    raise Http404("Bad request method")

def _update_form_attr(dbform, seafform):
    """Update db attributes from ODS file"""
    needupdate = False
    if seafform.title != dbform.title:
        dbform.title = seafform.title
        needupdate = True
    if seafform.description != dbform.description:
        dbform.description = seafform.description
        needupdate = True
    if seafform.public != dbform.public:
        dbform.public = seafform.public
        needupdate = True
    if needupdate:
        dbform.save()

def formview(request, formid, uuid=None):
    """Display a public form"""
    justaddedrow = None
    # get the form object
    try:
        form = Form.objects.get(formid=formid)
    except Form.DoesNotExist:
        raise Http404
    # get the seafform object (Seafile connexion)
    if settings.LOCAL:
        seaf = None
    else:
        seafu = form.owner.seafileuser
        seaf = SeafileClient(seafu.seafroot, verifycerts=settings.VERIFYCERTS, v12auth=V12AUTH)
        seaf.authenticate(form.owner.email, token=seafu.seaftoken, validate=False)
    seafform = SeafForm(form.filepath, seaf, form.repoid)
    try:
        seafform.load()
    except InvalidODS as inv:
        return handler500(request, inv.msg)
    except InvalidToken:
        return handler500(request, _('Seafile authentication invalid. The owner of the form need to re-authenticate.'))
    except APIError:
        raise Http404
    
    _update_form_attr(form, seafform)
    
    current_row = None
    
    # build the corresponding DjForm()
    if request.method == 'POST':
        results = False
        # results management
        sanitized_post=request.POST.copy()
        # this select the first answer for two fields named the same way
        # (usecase : editing data in the table view)
        for k in sanitized_post:
            sanitized_post[k] = sanitized_post.getlist(k)[0]
        
        # check if we delete a row
        if sanitized_post.get('delete', None) == "1":
            if not seafform.delete:
                # delete not authorized
                raise handler500(request, _('Delete an answer is not authorized in this form'))
            # delete row sanitized_post["rowid"]
            seafform.delete_row(int(sanitized_post["rowid"]) + HEADERS_ROW) 
            # then redirect to GET the result page
            return HttpResponseRedirect(
                reverse('form',args=(formid,)) + '?results'
            )
        
        djform = DjForm(sanitized_post, request.FILES, \
                        fieldlist=seafform.fields, \
                        form_url=request.build_absolute_uri())
        
        if djform.is_valid():
            # check if we replace a row
            if djform.cleaned_data['rowid'] != 'newrow':
                replace_row = int(djform.cleaned_data['rowid'])
                justaddedrow = replace_row - HEADERS_ROW
            else:
                replace_row = None
                justaddedrow = seafform._first_empty_row - HEADERS_ROW
            
            # check if we exceed the limit
            if seafform.limit:
                canceled_rows = []
                # make the list of canceled rows
                for colid in seafform.cancel_cols:
                    colid = colid - 1
                    for row_id in range(len(seafform.data)):
                        if seafform.data[row_id][colid] and row_id not in canceled_rows:
                            canceled_rows.append(row_id)
                nb_answer = len(seafform.data) - len(canceled_rows)
                
                if replace_row is None:
                    # can we add a row?
                    if nb_answer >= seafform.limit:
                        # invalidation of the form
                        djform.add_error(None, _("The maximum number of answer ({0}) is reached, you cannot add a new one.").format(seafform.limit))
                elif nb_answer >= seafform.limit:
                    # prevent to uncancel a canceled answer
                    for colid in seafform.cancel_cols:
                        old_cancel_value = seafform.data[justaddedrow][colid-1]
                        f_label = seafform.fields[colid-1].label
                        new_cancel_value = djform.cleaned_data[f_label]
                        if old_cancel_value and not new_cancel_value:
                            # cancel was unchecked, but there is no room for this
                            djform.add_error(None, _("The maximum number of answer ({0}) is reached, you cannot un-cancel this one.").format(seafform.limit))                            
        
        if djform.is_valid():
            # upload files
            for field in seafform.fields:
                if field.ident == 'upload':
                    try:
                        # envoi le fichier
                        seafform.upload_file(field, request.FILES[field.label], djform.cleaned_data, replace_row)
                    except MultiValueDictKeyError:
                        print('pas de fichier fourni pour', field.label)
            
            # save data
            seafform.post(djform.cleaned_data, replace_row)
            
            # if valid form and form and not edit:
            if seafform.view_as == 'form' and not seafform.edit:
                # redirect to thanks
                return HttpResponseRedirect(reverse('thanks',args=(formid,)))
            elif seafform.view_as == 'form':
                return HttpResponseRedirect(
                    reverse('form',args=(formid,)) + '?results'
                )
                #results = True # redirect to table
                
            # clean fields
            djform = DjForm(fieldlist=seafform.fields, \
                            form_url=request.build_absolute_uri())
            
    elif uuid:
        # load data from uuid
        # check if there is a editlink field and its position
        try:
            initials, files = seafform.get_values_from_uuid_url(request.build_absolute_uri())
        except KeyError:
            raise Http404
        for field in files:
            initials[field] = files[field] 
        djform = DjForm(
            initials,
            files,
            fieldlist=seafform.fields, 
            form_url=request.build_absolute_uri() 
        )
        results = ('results' in request.GET)
        current_row = initials['rowid'] - HEADERS_ROW
    else:
        djform = DjForm(fieldlist=seafform.fields, \
                        form_url=request.build_absolute_uri())
        results = ('results' in request.GET)
    
    # compute some results
    max_chk = 0
    computations = []
    canceled_rows = []
    
    # make the list of canceled rows
    for colid in seafform.cancel_cols:
        colid = colid - 1
        for row_id in range(len(seafform.data)):
            if seafform.data[row_id][colid] and row_id not in canceled_rows:
                canceled_rows.append(row_id)
    
    for colid, field in enumerate(seafform.fields):
        # if check boxe, number of checks
        if field.ident.startswith('check'):
            res = sum(
                row[colid] 
                for rowid, row in enumerate(seafform.data) 
                if row[colid] and rowid not in canceled_rows
            )
            max_chk = max(max_chk, res)
            computations.append(res)
        # if number, the sum
        elif field.ident == 'number':
            computations.append(sum(
                row[colid] 
                for rowid, row in enumerate(seafform.data) 
                if row[colid] and rowid not in canceled_rows 
            ))
        elif field.ident != 'cancel':
            computations.append(None)
        
    # columns with a star
    max_chk_column = []
    for colid, field in enumerate(seafform.fields):
        if field.ident.startswith('check') and computations[colid] == max_chk:
            max_chk_column.append(colid)
    
    # compute the answer number (remove canceled answer)
    nb_answer = len(seafform.data) - len(canceled_rows)
    
    if seafform.view_as == 'form' and not results:
        # form view
        return render(request, 'seafform/form_as_form.html', {
            'LOGOS': LOGOS,
            'seafform': seafform,
            'modelform': form,
            'djform': djform,
            'current_row': current_row,
            'canceled_rows': canceled_rows,
        })
        
    elif seafform.view_as == 'table' or (results and seafform.edit):
        # hightlight first column if static field
        first_is_static = (seafform.fields[0].ident == 'static')
        
        # table view    
        return render(request, 'seafform/form_as_table.html', {
            'LOGOS': LOGOS,
            'seafform': seafform,
            'modelform': form,
            'djform': djform,
            'justaddedrow': justaddedrow,
            'first_is_static': first_is_static,
            'computations': computations,
            'max_chk_column': max_chk_column,
            'canceled_rows': canceled_rows,
            'nb_answer' : nb_answer,
        })

    raise Http404

def formrowedit(request, formid, rowid):
    """Generate a form to edit rowid in formid"""
    rowid = int(rowid) + HEADERS_ROW
    # get the form object
    try:
        form = Form.objects.get(formid=formid)
    except Form.DoesNotExist:
        raise Http404
    # get the seafform object (Seafile connexion)
    if settings.LOCAL:
        seaf = None
    else:
        seafu = form.owner.seafileuser
        seaf = SeafileClient(seafu.seafroot, verifycerts=settings.VERIFYCERTS, v12auth=V12AUTH)
        seaf.authenticate(form.owner.email, token=seafu.seaftoken, validate=False)
    seafform = SeafForm(form.filepath, seaf, form.repoid)
    seafform.load()
    # create the django form for a specific row
    initials = seafform.get_values_from_data(rowid)
    initials['rowid'] = rowid
    files = seafform.get_files_from_data(rowid)
    for field in files:
        initials[field] = files[field]
    djform = DjForm(
        initials,
        files,
        fieldlist=seafform.fields,
        form_url=request.build_absolute_uri()
    )
    return render(request, 'seafform/rowedit.html', {
        'seafform': seafform,
        'djform': djform,
        'modelform': form,
        'rowid': rowid - HEADERS_ROW,
    })

def thanks(request, formid):
    """Display the Thanks page"""
    # get the form object
    try:
        form = Form.objects.get(formid=formid)
    except Form.DoesNotExist:
        raise Http404
    return render(request, 'seafform/thanks.html', {
        'LOGOS': LOGOS,
        'seafform': form,
    })

def handler404(request, exception):
    response = render(request, '404.html', {'LOGOS': LOGOS})
    response.status_code = 404
    return response

def handler500(request, error_message=None):
    response = render(request, '500.html', {
        'LOGOS': LOGOS,
        'error_message':error_message,
    })
    response.status_code = 500
    return response
