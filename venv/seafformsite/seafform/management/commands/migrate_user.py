# -*- coding: utf-8 -*-
###############################################################################
#       seafform/management/migrate_user.py
#       
#       Copyright © 2023, Florence Birée <florence@biree.name>
#       
#       This file is a part of seafform.
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as 
#       published by the Free Software Foundation, either version 3 of the 
#       License, or (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU Affero General Public License for more details.
#       
#       You should have received a copy of the GNU Affero General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Seafform management: migrate user"""

__author__ = "Florence Birée"
__version__ = "0.7.1"
__license__ = "AGPLv3"
__copyright__ = "Copyright © 2023, Florence Birée <florence@biree.name>"

from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from seafform.models import Form
from django.db.utils import IntegrityError

class Command(BaseCommand):
    help = "Migrate the `email` (user ID) used by Seafile (equivalent to the migrate API)"

    def add_arguments(self, parser):
        parser.add_argument("from_email")
        parser.add_argument("to_email")

    def handle(self, *args, **options):
        try:
            user = User.objects.get(username=options["from_email"])
        except User.DoesNotExist:
            raise CommandError('User {} does not exist'.format(options["from_email"]))
        
        user.username = options["to_email"]
        user.email = options["to_email"]
        try:
            user.save()
        except IntegrityError:
            newuser = User.objects.get(username=options["to_email"])
            # the user alreay exists… move polls
            for form in Form.objects.filter(owner=user):
                form.owner = newuser
                form.save()
            # delete the old user
            user.delete()
            self.stdout.write(
                self.style.SUCCESS('{} user already exists, all form migrated, {} deleted.'.format(options["to_email"], options["from_email"]))
            )
        else:
            self.stdout.write(
                self.style.SUCCESS('Successfully updated user (now {})'.format(options["to_email"]))
            )

