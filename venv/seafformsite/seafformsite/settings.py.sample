"""
Django settings for seafformsite project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
# you can produce it by : $ openssl rand -base64 50 | head -c 50
SECRET_KEY = 'put here a 50 chars long random key'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['forms.example.org'] # Must be changed!
CSRF_TRUSTED_ORIGINS = ['https://forms.example.org'] # Must be changed

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin.apps.SimpleAdminConfig',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'seafform',
    'bootstrapform',
)

MIDDLEWARE = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'seafformsite.urls'

WSGI_APPLICATION = 'seafformsite.wsgi.application'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            # insert your TEMPLATE_DIRS here
            'SEAFFORM_INSTALL_PATH/venv/seafformsite/seafform/templates/'
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                # Insert your TEMPLATE_CONTEXT_PROCESSORS here or use this
                # list if you haven't customized them:
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.request',
            ],  
        },  
    },  
]

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'fr-fr'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/
STATIC_URL = '/static/'
STATIC_ROOT = '/path/to/static'

# HTTPS settings
CSRF_COOKIE_SECURE = True
SESSION_COOKIE_SECURE = True
# if seafform is on the host than seafile, it's better to change those names: 
CSRF_COOKIE_NAME="csrftoken_seafform"
CSRF_HEADER_NAME="HTTP_X_CSRFTOKEN_SEAFFORM"
SESSION_COOKIE_NAME="sessionid_seafform"


DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

# Seafform settings
SEAFILE_ROOT = 'https://seafile.example.org/'
TPL_URL = 'https://seafile.example.org/d/908b45b3b6/'
ROOT_URL = 'https://forms.example.org/'
# if seafform is not a the root of the domain, set the subdir without leading /
#ROOT_DIR = 'subdir/'
VERIFYCERTS = True
ALLOW_PUBLIC = False
PUBLIC_NEED_AUTH = True
SEAFILE_OFFICE_EDIT = False
# URL of your custom logo (32px, 150px, 300px)
#LOGO32 = ''
#LOGO150 = ''
#LOGO300 = ''
# If you want to set a messages on forms-editor pages (None to remove)
ADMIN_MSG = None
# Switch to Seafile-session login, need Seafform to be on the same domain than
# Seafile. This settings allow an oauth-compatible login
SEAFILE_SESSION_LOGIN = True
# Use Seafile >= v12 API authentication (new default from Seafform v0.7.2)
V12AUTH = True

# Customize the login label to follow your seafile config (default : Username or e-mail)
#LOGIN_LABEL = "Username"

# Seafform dev setting
LOCAL = False
LOCAL_ROOT = '/local/root/'

