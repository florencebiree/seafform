* v0.7.2
    - add support for new Seafile v12 API authentication
* v0.7.1
    - fix SEAFILE_SESSION_LOGIN Referer bug
    - add two ./manage.py commands: set_admin and migrate_user
* v0.7.0
    - allow to authenticate using either email, login_id or contact_email
    - allow to delegate authentication to Seafile (see SEAFILE_SESSION_LOGIN)
* v0.6.1
    - fix a bug for fields with trailing slashes
    - fix css for checkboxes in view_as_table
* v0.6
    - cosmetic improvments in forms
    - bugfix: unchecking a check box is now saved
    - bugfix: fix table editing when a longtext is required
    - now use Django 3.2
    - add an option to allow to cancel a row (not deleted, but not counted anymore)
    - add an option to allow to delete a row
    - add an option to limit the number of answer
    - add a field to upload files (saved in Seafile) from a form
    - add the possibility for admins to display a message in welcome and private pages
* v0.5
    - allow to see and edit only our own entries, not other (using editLink fields)
* v0.4
    - compatibility changes for Django 2.2 and Debian Buster
    - use seafile_webapi.py from https://framagit.org/florencebiree/python-seafile-webapi
    - improve the administration site
    - if an office app is integrated with Seafile, add an edit link in the private page
    - add a reload button to reload title, description and public status
    - change default logo, and add a setting to customize the logo
    - support password changes in Seafile -> update SeafForm account on next login
    - in table forms, fix the header, and fix the horizontal scrollbar
    - better fields alignment in the table view
* v0.3
    - fix the new form link color
    - when usal errors are found in the ODS file, explain it on the error page
    - bugfix: when two columns have the same name, only the first one is displayed
    - improve text next to "choose the file" button
    - update french translation
    - update git link for framagit

* v0.2
    - allow to ignore TLS certs check
    - allow forms to be public (with or without authentication)

* v0.1 - 26 sept 2015
    - initial release
