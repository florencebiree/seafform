# Seafile forms - upgrade instructions

## Upgrading from an older SeafForm version

Check first the dependency, especially the django version installed on your
system.

If you get the code using git, you can update it using:

    $ git pull
    $ git checkout tags/v0.7

You must take a look on venv/seafformsite/seafformsite/settings.py.
It may be simpler to make a backup of your old settings.py, copy the new
settings.py.sample to settings.py, and retreive your settings from the old one.

Some mandatory settings changes are listed here :

 - from v<0.7: you need to add CSRF_TRUSTED_ORIGINS to use django >= 4

 - from v<0.6: you need to add DEFAULT_AUTO_FIELD and ADMIN_MSG to your
   settings.py ; templates have changed, you should consider updating your
   Seafile library.

 - from v<0.5: templates and example have changed, you should consider
   updating your Seafile library.

 - from v<0.4: you need to add SEAFILE_OFFICE_EDIT and STATIC_ROOT 
   settings

Then, you must update your database scheme :

    $ cd venv/
    $ . bin/activate                                                                 
    $ cd seafformsite                                                                
    $ ./manage.py migrate

and the static files :

    $ ./manage.py collectstatic

## Upgrading your virtual environment after a system update

When you upgrade your system, you may change your Python version. This can
break your virtual environment, and thus SeafForm.

You need to re-create your virtual environment:

    $ virtualenv --python=python3 --system-site-packages venv/                   
    $ cd venv/ # go to the virtual env to install python packages                
    $ . bin/activate                                                             
    $ pip install ezodf gunicorn # and maybe other dependencies


                 
